<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Menu;

class Delete extends \Magento\Backend\App\Action
{
    protected $helper;
    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;

    /**
     * Delete constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository,
        \Isobar\Megamenu\Helper\Data $helper
    ) {
        $this->helper = $helper;
        $this->megaMenuRepository = $megaMenuRepository;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        $rootId = $this->getRequest()->getParam('root_id');
        if ($id) {
            try {
                // init model and delete
                $this->_deleteChildItems($rootId, $id);
                $this->megaMenuRepository->deleteById($id);

                // display success message
                $this->messageManager->addSuccess(__('You deleted the mega menu.'));
                // go to grid
                return $resultRedirect->setPath('*/rootmenu/manageitems', ['root_id' => $rootId]);
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/rootmenu/manageitems', ['root_id' => $rootId]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a mega menu to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/rootmenu/manageitems', ['root_id' => $rootId]);
    }

    protected function _deleteChildItems($rootId, $id)
    {
        $items = $this->helper->getMenuItemsByRootId($rootId);
        $treeData = $this->helper->getMenuItemTreeData($items);
        $this->_deleteRecursive($id, $treeData);
    }

    protected function _deleteRecursive($parentId, $treeData, $level = -1)
    {
        $level ++;
        if (isset($treeData['parents'][$parentId])) {
            foreach ($treeData['parents'][$parentId] as $itemId) {
                $treeData['items'][$itemId]->delete();
                if (in_array($itemId, $treeData['parents'][$parentId])) {
                    $this->_deleteRecursive($itemId, $treeData, $level);
                }
            }
        }
    }
}
