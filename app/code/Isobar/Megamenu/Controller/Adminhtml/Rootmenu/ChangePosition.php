<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Rootmenu;

class ChangePosition extends \Magento\Backend\App\Action
{
    protected $megaMenuFactory;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megaMenuFactory
    ) {
        $this->megaMenuFactory = $megaMenuFactory;
        parent::__construct($context);
    }

    /**
     * Create new Isobar Megamenu
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $post = $this->getRequest()->getPost();
        $afterChange = $post['json'];
        if (!isset($post['old'])) {
            $beforeChange = json_encode(array());
        } else {
            $beforeChange = $post['old'];
        }
        $afterChange = json_decode($afterChange, true);
        $beforeChange = json_decode($beforeChange, true);
        $this->_updateTree($afterChange);
        return;
    }

    protected function _updateTree($afterChangeValue, $parent = 0)
    {
        $i = 1;
        foreach ($afterChangeValue as $key => $value) {
            $this->_updatePosition($value['id'], $i);
            if (isset($value['children'])) {
                $this->_updateTree($value['children'], $value['id']);
            }
            $this->_setParent($value['id'], $parent);
            $i += 1;
        }
    }

    protected function _updatePosition($id, $pos)
    {
        $item = $this->megaMenuFactory->create();
        $item->load($id);
        if ($item->getId()) {
            $item->setSort($pos);
            $item->save();
        }
    }

    protected function _setParent($menuItemId, $parentId)
    {
        $item = $this->megaMenuFactory->create();
        $item->load($menuItemId);
        $item->setParentId($parentId);
        $item->save();
    }
}
