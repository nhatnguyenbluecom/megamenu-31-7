<?php

namespace Isobar\Megamenu\Block\Adminhtml\Rootmenu\Manage\Edit;


class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected $rootId;

    /**
     * @var \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory
     */
    protected $megaMenuFactory;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;

    protected $rootMenuRepository;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\SortOrder
     */
    protected $sortOrder;

    public $helper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megaMenuFactory,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository,
        \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuRepository,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrder $sortOrder,
        \Isobar\Megamenu\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->rootMenuRepository = $rootMenuRepository;
        $this->megaMenuFactory = $megaMenuFactory;
        $this->megaMenuRepository = $megaMenuRepository;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrder = $sortOrder;
        parent::__construct($context, $registry, $formFactory, $data);
        $this->rootId = $this->getRequest()->getParam('root_id');
    }

    protected function buildTree($parentId, $treeData, $level = -1)
    {
        $html = '<ol class="dd-list">';
        $level++;
        if (isset($treeData['parents'][$parentId])) {
            $index = 0;
            foreach ($treeData['parents'][$parentId] as $itemId) {
                $rootId = $this->getRequest()->getParam('root_id');
                $editUrl = $this->getUrl('*/menu/edit', array('root_id' => $this->rootId, 'id' => $itemId));
                $parenId = $parentId;

                $urlDel = $this->getUrl('*/menu/delete', array('root_id' => $this->rootId, 'id' => $itemId));

                if (in_array($itemId, $treeData['parents'][$parentId])) {
                    $html .= '<li data-id="'.$itemId.'" class="dd-item parent">
                        <div class="dd-handle">
                             <i class="btn-move" title="Drag"></i>
                            <span>'.$treeData['items'][$itemId]->getTitle().'</span>
                            <span class="actions">
                                <span class="accept"><a href="'.$editUrl.'"  class="edit-icon" title="Edit">Edit</a></span>&nbsp;
                                <a href="'.$urlDel.'" onclick="return xn();" class="delete-icon" title="Delete">
                                    Delete
                                </a>
                            </span>
                        </div>
                        '
                        ;
                    $html .= $this->buildTree($itemId, $treeData, $level).'</li>';
                }
            }
        }
        $html .= '</ol>';
        return $html;
    }

    public function renderMenuItemHierachy()
    {
        $items = $this->helper->getMenuItemsByRootId($this->getRequest()->getParam('root_id'));
        $treeData = $this->helper->getMenuItemTreeData($items);
        return $this->buildTree(0, $treeData);
    }

    public function getChangePositionUrl()
    {
        return $this->getUrl('megamenu/*/changeposition');
    }
}
